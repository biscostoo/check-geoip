# check-geoip - README
Lo script esegue la verifica di geolocalizzazione per determinati IP.
Gli indirizzi IP vengono presi da un file di testo che di default si chiama "listaip.txt" e scrive l'output in "risultato.txt".
Questi due file sono inseriti in variabili ed e' possibile modificarne i nomi ed i path.

# Dipendenze
Lo script ha le seguenti dipendenze (in ordine alfabetico):
- bash
- cat
- cut
- egrep
- lynx 
- sed
- tr
