#!/usr/bin/env bash
# Lo script recupera informazioni di geolocalizzazione degli IP attraverso il sito http://whatismyipaddress.com
# Autore dello scempio: bisco

# Variabili - inserire i file di input per gli ip e l'output
IP_LIST=listaip.txt
OUTPUT=risultato.txt
BROWSER=`which lyn`
# Fine Variabili

# Eseguo la verifica che esista il file di input
if [ ! -e ${IP_LIST} ]; then
    echo "Inserire gli indirizzi IP uno per riga e terminare con EOF"
    while true;
    do
        read IPADDR
        echo ${IPADDR} >> $IP_LIST
        if [ "$IPADDR" = "EOF" ];
        then
            break
        fi
    done
fi

# Eseguo la verifica che esista lynx
if [ -e "${BROWSER}" ];
then 
    echo "lynx e' correttamente installato"
else
    echo "E' necessario installare lynx"
    exit 0
fi

# Codice
for IP in `cat ${IP_LIST} | grep -v EOF`; do
	echo "Eseguo la ricerca per ${IP}";
	echo ${IP} : `lynx -dump http://whatismyipaddress.com/ip/${IP} | egrep '(City|State|Country)' | cut -d : -f 2 | sed s/$/\;\ / | sed 's/^[ \t]*//;s/[ \t]*$//' | tr -d '\n'` >> ${OUTPUT};
done
